package Animals;

import java.util.Objects;

public class Predator extends Mammal{
   private boolean isScavenger;

    public Predator(String nickName, double size, int pregnantPeriod, boolean isScavenger) {
        super(nickName, size, pregnantPeriod);
        this.isScavenger = isScavenger;

    }

    public String isScavenger() {
        return ", is scavenger : " +isScavenger;
    }


    @Override
    public void say() {
        System.out.println("Rrrrr!!!!!1");
    }
}
