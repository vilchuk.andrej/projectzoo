package Animals;

public class DomesticCat extends Feline {
    private String breed;
    public DomesticCat(String nickName, double size, int pregnantPeriod, boolean isScavenger,String breed) {
        super(nickName, size, pregnantPeriod, isScavenger);
        this.breed = breed;
    }

    public String getBreed() {
        return ",Breed :"+breed;
    }

    @Override
    public void say() {
        System.out.println("Meowwww) ");
    }


}
