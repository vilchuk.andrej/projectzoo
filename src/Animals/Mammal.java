package Animals;

public class Mammal extends Animal {
    private int pregnantPeriod ;

    public Mammal(String nickName, double size,int pregnantPeriod) {
        super(nickName, size);
        this.pregnantPeriod = pregnantPeriod;
    }

    public String getPregnantPeriod() {
        return " Pregnant period: " +pregnantPeriod;
    }

    @Override
    public String toString() {
        return "Mammal{" +
                "pregnantPeriod=" + pregnantPeriod +
                '}';
    }

    @Override
    public void say() {
        System.out.println("No sound found for mammal");
    }
}
