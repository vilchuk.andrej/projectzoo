package Animals;

public class ForestWolf extends Canine {
    public ForestWolf(String nickName, double size, int pregnantPeriod, boolean isScavenger) {
        super(nickName, size, pregnantPeriod, isScavenger);
    }

    @Override
    public void say() {
        System.out.println("Yffff!!1111");
    }
}
