package Animals;

public class Feline extends Predator {
    public Feline(String nickName, double size, int pregnantPeriod, boolean isScavenger) {
        super(nickName, size, pregnantPeriod, isScavenger);
    }

    @Override
    public void say() {
        System.out.println("No sound find for feline");
    }
}

