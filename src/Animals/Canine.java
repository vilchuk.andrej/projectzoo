package Animals;

public class Canine extends Predator{

    public Canine(String nickName, double size, int pregnantPeriod, boolean isScavenger) {
        super(nickName, size, pregnantPeriod, isScavenger);
    }

    @Override
    public String isScavenger() {
        return super.isScavenger();
    }

    @Override
    public String getPregnantPeriod() {
        return super.getPregnantPeriod();
    }

    @Override
    public String getNickName() {
        return super.getNickName();
    }

    @Override
    public void say() {
        System.out.println("Gavvv- Gavvv");
    }
}
