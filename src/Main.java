import Animals.*;

public class Main {
    public static void main(String[] args) {
        Animal [] DomesticCat =  new Animal[3];
        Animal animal = new Animal("Ted", 12);
            System.out.println(animal.getNickName());
            animal.say();
            System.out.println();
        DomesticCat cat1 = new DomesticCat("Kolya",2.2,0,false,"Milk");
        DomesticCat cat2 = new DomesticCat("Ronaldo",7.5,4,false,"Milk");
        DomesticCat cat3 = new DomesticCat("Donald",10.5,3,false,"Milk");
        DomesticCat[0] = cat1;
        DomesticCat[1] = cat2;
        DomesticCat[2] = cat3;
        for (int i =0; i<3;i++){
                System.out.println("Cat " + (i+1) + " " + DomesticCat[i]);
        }


        Animal bird = new Bird("Kesha", 4);
        System.out.println(bird.getNickName());
        bird.say();
        System.out.println();
        Mammal mammal = new Mammal("Henri", 5.8, 4);
        System.out.println(mammal.getNickName() + mammal.getPregnantPeriod());
        mammal.say();
        System.out.println();
        Predator predator = new Predator("Leo", 5, 3, true);
        System.out.println(predator.getNickName() + predator.getPregnantPeriod() + predator.isScavenger());
        predator.say();
        System.out.println();
        Canine canine = new Canine("Boba", 9.3, 0, false);
        System.out.println(canine.getNickName() + canine.getPregnantPeriod() + canine.isScavenger());
        canine.say();
        System.out.println();
        Feline feline = new Feline("Georgij", 2.2, 6, true);
        System.out.println(feline.getNickName() + feline.getPregnantPeriod() + feline.isScavenger());
        feline.say();
        System.out.println();
        ForestWolf forestWolf = new ForestWolf("Leonid", 14.5, 0, true);
        System.out.println(forestWolf.getNickName() + forestWolf.getPregnantPeriod() + forestWolf.isScavenger());
        forestWolf.say();
        System.out.println();
        DomesticCat domesticCat = new DomesticCat("Myrzik", 1, 0, false,
                "Milk");
        System.out.println(domesticCat.getNickName() + domesticCat.getPregnantPeriod() + domesticCat.isScavenger() +
                domesticCat.getBreed());
        domesticCat.say();
        System.out.println();
    }
}
